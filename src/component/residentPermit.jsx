import { Component, UIText } from "rainbowui-desktop-core";
import PropTypes from 'prop-types';
import '../css/component.css';
import { ValidatorContext } from 'rainbow-desktop-cache';
import customerValidator from './customerValidator';
import { r18n } from "rainbow-desktop-tools";

export default class TwResidentPermit extends Component {
    constructor(props) {
        super(props)
        this.state = {
        }
        this.id = "residentRermit" + this.generateId()
    }

    render() {
        let labelNoI18n = this.props.label ? false : true;
        return (
            <div id="desktop-resident-permit">
                <UIText id={this.id} label={this.props.label ? this.props.label : r18n.residentPermit} colspan={this.props.colspan}
                    noI18n={labelNoI18n} layout='horizontal' model={this.state} property="ResidentPermitNo" required={this.state.required ? this.state.required : 'false'}
                    maxLength="10" validator={customerValidator} onKeyUp={this.onKeyUp.bind(this)} onBlur={this.onBlur.bind(this)}
                    toUpperLowerCase="upper" />
            </div>
        )
    }

    onKeyUp() {
        if (this.state.ResidentPermitNo && this.state.ResidentPermitNo.length == 10) {
            ValidatorContext.validateField(this.id);
            this.props.model.ResidentPermitNo = this.state.ResidentPermitNo;
        }
    }

    onBlur() {
        let upResidentPermitNo = this.state.ResidentPermitNo ? this.state.ResidentPermitNo.toUpperCase() : '';
        this.props.model.ResidentPermitNo = upResidentPermitNo;
        if (this.props.onBlur) {
            this.props.onBlur()
        }
    }

    generateId() {
        let index = new Date().getTime();
        return "rainbow-ui-" + (index++);
    }
};

TwResidentPermit.propTypes = $.extend({}, Component.propTypes, {
    label: PropTypes.string,
    required: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
});


TwResidentPermit.defaultProps = $.extend({}, Component.defaultProps, {
    colspan: "1",
    required: false
});
