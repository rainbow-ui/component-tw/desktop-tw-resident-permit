import {ValidatorContext} from 'rainbow-desktop-cache';
import {r18n} from "rainbow-desktop-tools";

const InputValidator = {

    validate: function(component){
        let flag = false;
        let validator = {
            // notEmpty: {
            //     message: '字段不能是空'
            // },
            stringLength: {
                min: 10,
                max: 10,
                message: r18n.stringLength
            },
            regexp: {
                regexp: /^[a-zA-Z]{1}[a-fA-F]{1}[0-9]{8}$/,
                message: r18n.twResidentRegexp
            }
        };
        ValidatorContext.putValidator(component.getValidationGroup(), component.componentId, validator);
    }
	

};

module.exports = InputValidator;
